#include "fileWatch.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <dirent.h>
#include <string.h>
#include <errno.h>
#include <sys/inotify.h>
#include <sys/stat.h>
#include <time.h>
#include "intToPtrTree.h"

static Tree_Type intToPtrTree = NULL;

static void watchDirAndSubdirs(char *dirname, int notifyFD, int triggerOnExisting)
{
    DIR *thisDir = opendir(dirname);
    struct dirent *directoryEntry;
    int watchNum;
    char *oldWatchDir;
    if (!thisDir)
    {
        fprintf(stderr, "Can't open directory %s: %s\n", dirname, strerror(errno));
        return;
    }
    /* At this point we know we are looking at a directory */
    watchNum = inotify_add_watch(notifyFD, dirname, IN_ALL_EVENTS);
    if (watchNum == -1)
    {
        fprintf(stderr, "Can't watch directory %s: %s\n", dirname, strerror(errno));
        closedir(thisDir);
        return;
    }
    oldWatchDir = intToPtrTreeInsert(intToPtrTree, watchNum, strdup(dirname));
    if (oldWatchDir) /* watchNum already in tree */
    {
       if (!strcmp(oldWatchDir, dirname))
       {
           /* Directory was already being watched */
           fprintf(stderr, "Was already watching %s\n", dirname);
           free(oldWatchDir);
           closedir(thisDir);
           return;
       }
       else
       {
           /* watchNum assigned to new directory */
           /* This means the old watch was deleted and we didn't detect it */
           fprintf(stderr, "Somehow we missed the removal of watch for %s\n", oldWatchDir);
           free(oldWatchDir);
       }
    }
    /* fprintf(stdout, "(%3d) Watching %s\n", watchNum, dirname); */
    /* At this point events will be delivered for this directory.  However, anything that
     * happened in this directory between what triggered this function and the creation of
     * the watch will be missed.  So, triggerOnExisting set to non zero will cause an
     * event to be created for existing files. */
    while ((directoryEntry = readdir(thisDir)))
    {
        char subdir[FILENAME_MAX];
        struct stat statBuf;
        if (directoryEntry->d_name[0] == '.')
            continue;
        snprintf(subdir, sizeof subdir, "%s/%s", dirname, directoryEntry->d_name);
        if (lstat(subdir, &statBuf) == 0 && S_ISDIR(statBuf.st_mode))
        {
            watchDirAndSubdirs(subdir, notifyFD, triggerOnExisting);
        }
        else if (triggerOnExisting)
        {
            /* TODO Somehow touch the file to generate an event */
        }
    }
    closedir(thisDir);
}

int fileWatch(char **directories, int events, FileWatchCallback fileWatchCallback)
{
    char charBuffer[sizeof(struct inotify_event) + FILENAME_MAX + 1];
    int notifyFD, notifyReadLen;
    char **dirs = directories;

    intToPtrTree = intToPtrTreeAllocate();
    if (!intToPtrTree)
    {
        fprintf(stderr, "**Could not allocate storage tree\n");
        return 1;
    }
    notifyFD = inotify_init();
    if (notifyFD == -1)
    {
        fprintf(stderr, "**inotify_init: %s", strerror(errno));
        return 1;
    }
    while (*dirs)
    {
        watchDirAndSubdirs(*dirs, notifyFD, 0);
        dirs++;
    }

    while ((notifyReadLen = read(notifyFD, charBuffer, sizeof charBuffer)) > 0 || errno == EINTR)
    {
        char *p = charBuffer;
        if (notifyReadLen <= 0) continue; /* EINTR happened */
        while (p < charBuffer + notifyReadLen)
        {
            struct inotify_event *event = (struct inotify_event *)p;
            if (!(event->mask & IN_IGNORED))
            {
                if (event->mask & IN_ISDIR && event->mask & IN_CREATE)
                {
                    char newDir[FILENAME_MAX];
                    snprintf(newDir, sizeof newDir, "%s/%s",
                            intToPtrTreeLookup(intToPtrTree, event->wd), event->name);
                    watchDirAndSubdirs(newDir, notifyFD, 1);
                }
                else if (event->mask & IN_DELETE_SELF)
                {
                    char *oldDir = intToPtrTreeRemove(intToPtrTree, event->wd);
                    if (oldDir)
                    {
                        /* fprintf(stdout, "(%3d) Deleting %s\n", event->wd, oldDir); */
                        inotify_rm_watch(notifyFD, event->wd);
                        free(oldDir);
                    }
                }
                else
                {
                    if (!(event->mask & IN_ISDIR) && (event->mask & events))
                    {
                        char filename[FILENAME_MAX];
                        snprintf(filename, sizeof filename, "%s/%s",
                                intToPtrTreeLookup(intToPtrTree, event->wd),
                                event->name);
                        fileWatchCallback(filename, event->mask);
                    }
                }
            }
            p += sizeof(struct inotify_event) + event->len;
        }
    }
    return 0;
}
