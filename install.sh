#!/bin/sh
set -e
if [ `whoami` = root ] ; then
    PREFIX=/usr/local
else
    PREFIX=${HOME}/`uname -r | grep -o el.`_local
fi
echo -n Please enter parent directory [$PREFIX]:
read pref
if [ "$pref" != "" ] ; then
    PREFIX=$pref
fi
if [ ! -d $PREFIX ] ; then
    echo -n "Create $PREFIX?"
    read y
    if [ "$y" != y ] ; then
        exit
    fi
    mkdir -p $PREFIX
fi
make
echo cp fileWatch.h    $PREFIX/include
cp fileWatch.h    $PREFIX/include
echo cp libfileWatch.a $PREFIX/lib
cp libfileWatch.a $PREFIX/lib
echo cp sampleWatcher  $PREFIX/bin
cp sampleWatcher  $PREFIX/bin
