#include <stdio.h>
#include <stdlib.h>
#include "tree.h"
#include "intToPtrTree.h"

typedef struct IntPtr
{
    int   number; /* the label for the tree */
    char *pointer;
} IntPtr;

static Tree_Label_Type labelIntPtr(Tree_Item_Type Item)
{
    IntPtr *intPtr = (IntPtr *)Item;
    return &(intPtr->number);
}

static int compareIntPtr(Tree_Label_Type Left, Tree_Label_Type Right)
{
    int l = *(int *)Left;
    int r = *(int *)Right;
    if (l < r) return -1;
    if (l > r) return 1;
    return 0;
}

Tree_Type intToPtrTreeAllocate(void)
{
    return Tree_Allocate(
            Label_Delegate(labelIntPtr),
            Compare_Delegate(compareIntPtr),
            NULL);
}

char *intToPtrTreeInsert(Tree_Type intToPtrTree, int number, char *pointer)
{
    IntPtr *intPtr = (IntPtr *)malloc(sizeof(IntPtr));
    intPtr->number = number;
    intPtr->pointer = pointer;
    if (Tree_Insert_Item(intToPtrTree, intPtr))
    {
        return NULL; /* Nothing was already there */
    }
    else
    {
        char *oldVal = intToPtrTreeRemove(intToPtrTree, number);
        Tree_Insert_Item(intToPtrTree, intPtr);
        return oldVal;
    }
}

char *intToPtrTreeLookup(Tree_Type intToPtrTree, int number)
{
    IntPtr *item = (IntPtr *)Tree_Find_Item(intToPtrTree, &number);
    if (item)
        return item->pointer;
    else
        return NULL;
}

char *intToPtrTreeRemove(Tree_Type intToPtrTree, int number)
{
    IntPtr *oldItem = (IntPtr *)Tree_Remove_Item(intToPtrTree, &number);
    if (oldItem)
    {
        char *oldPtr = oldItem->pointer;
        free(oldItem);
        return oldPtr;
    }
    return NULL;
}
