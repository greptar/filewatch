###############################################################################
#   Name:       Makefile - Make file for LIS ISS compiled software
#
#   Usage:      make
#
#   Description:
#       Make file for LIS ISS compiled software.
#
#   Notes:
#       None.
#
#   Revision history:
#       2015-07-14 BEB Written
#       2016-04-15 NAW Simplified, moved dependencies to separate files
###############################################################################

# In this first section, set:
#   MAINS:
#     The names of the executables to build.  It is assumed that the sources
#     are in the names appended with .c
#   LIBNAME:
#     The name of the library made with the other source files in this directory
#   CFLAGS:
#     The flags passed to the C compiler when compiling all the C code
#   LDLIBS:
#     The -l options needed to link the mains
#   LDFLAGS:
#     The -L options needed to find the above libs
#   CPPFLAGS:
#     The -D and -I options to find any external includes
#   TARGET_ARCH:
#     Could set to -m32 to build 32 bit objects.  Need to install 32 bit development rpms.

MAINS      = sampleWatcher
LIBNAME    = libfileWatch.a
CC         = gcc
CFLAGS     = -Wall -ggdb3 -fPIC
CPPFLAGS   = -D_GNU_SOURCE
LDFLAGS    = 
LDLIBS     = 

SOURCES    = $(wildcard *.c)
OBJECTS    = $(patsubst %.c,%.o,$(SOURCES))
DEPFILES   = $(patsubst %.c,%.d,$(SOURCES))
SHAREDOBJ  = $(patsubst %.a,%.so,$(LIBNAME))

MAIN_SRCS  = $(addsuffix .c,$(MAINS))

LIB_SRCS   = $(filter-out $(MAIN_SRCS), $(SOURCES))
LIB_OBJS   = $(patsubst %.c,%.o,$(LIB_SRCS))

.PHONY     : all clean
all        : $(MAINS) $(SHAREDOBJ)
$(MAINS)   : $(LIBNAME)
$(LIBNAME) : $(LIB_OBJS)
	$(AR) $(ARFLAGS) $@ $?
$(SHAREDOBJ) : $(LIB_OBJS)
	$(CC) $(CFLAGS) -shared -o $(SHAREDOBJ) $(LIB_OBJS)
clean      :
	rm -f $(MAINS) $(LIBNAME) $(DEPFILES) $(OBJECTS) $(SHAREDOBJ)

%.d : %.c
	$(CC) $(CPPFLAGS) -MM -MT $@ -MT $(patsubst %.d,%.o,$@) $< > $@

ifneq "$(MAKECMDGOALS)" "clean"
-include $(DEPFILES)
endif
