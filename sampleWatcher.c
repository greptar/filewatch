#include "fileWatch.h"

#include <sys/inotify.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

/*
 * This is a sample of using the fileWatch library.
 *
 * Here is a sample of the output of this program:
 *
 * [nwharton@gust fileWatch]$ ./sampleWatcher &
 * No arguments given.  Watching /home/nwharton/ghrc-iss-lis/fileWatch.
 * [nwharton@gust fileWatch]$ echo hello > t
 * 2016/10/24 16:19:42 IN_CREATE        /home/nwharton/ghrc-iss-lis/fileWatch/t
 * 2016/10/24 16:19:42 IN_OPEN          /home/nwharton/ghrc-iss-lis/fileWatch/t
 * 2016/10/24 16:19:42 IN_MODIFY        /home/nwharton/ghrc-iss-lis/fileWatch/t
 * 2016/10/24 16:19:42 IN_CLOSE_WRITE   /home/nwharton/ghrc-iss-lis/fileWatch/t
 * [nwharton@gust fileWatch]$ touch t
 * 2016/10/24 16:19:48 IN_OPEN          /home/nwharton/ghrc-iss-lis/fileWatch/t
 * 2016/10/24 16:19:48 IN_ATTRIB        /home/nwharton/ghrc-iss-lis/fileWatch/t
 * 2016/10/24 16:19:48 IN_CLOSE_WRITE   /home/nwharton/ghrc-iss-lis/fileWatch/t
 * [nwharton@gust fileWatch]$ cat t
 * 2016/10/24 16:19:54 IN_OPEN          /home/nwharton/ghrc-iss-lis/fileWatch/t
 * hello
 * 2016/10/24 16:19:54 IN_ACCESS        /home/nwharton/ghrc-iss-lis/fileWatch/t
 * 2016/10/24 16:19:54 IN_CLOSE_NOWRITE /home/nwharton/ghrc-iss-lis/fileWatch/t
 * [nwharton@gust fileWatch]$ mv t ..
 * 2016/10/24 16:20:10 IN_MOVED_FROM    /home/nwharton/ghrc-iss-lis/fileWatch/t
 * [nwharton@gust fileWatch]$ mv ../t .
 * 2016/10/24 16:20:15 IN_MOVED_TO      /home/nwharton/ghrc-iss-lis/fileWatch/t
 * [nwharton@gust fileWatch]$ rm t
 * 2016/10/24 16:20:16 IN_DELETE        /home/nwharton/ghrc-iss-lis/fileWatch/t
 *
 */

static char *exitOnDelete = NULL;

static void fileWatchCallback(char *filename, int event)
{
    char outstr[64], *eventType;
    time_t t;
    struct tm *tmp;
    t = time(NULL);
    tmp = localtime(&t);
    strftime(outstr, sizeof(outstr), "%Y/%m/%d %H:%M:%S", tmp);
         if (event & IN_ACCESS       ) eventType = "IN_ACCESS       ";
    else if (event & IN_ATTRIB       ) eventType = "IN_ATTRIB       ";
    else if (event & IN_CLOSE_WRITE  ) eventType = "IN_CLOSE_WRITE  ";
    else if (event & IN_CLOSE_NOWRITE) eventType = "IN_CLOSE_NOWRITE";
    else if (event & IN_CREATE       ) eventType = "IN_CREATE       ";
    else if (event & IN_DELETE       ) eventType = "IN_DELETE       ";
    else if (event & IN_MODIFY       ) eventType = "IN_MODIFY       ";
    else if (event & IN_MOVED_FROM   ) eventType = "IN_MOVED_FROM   ";
    else if (event & IN_MOVED_TO     ) eventType = "IN_MOVED_TO     ";
    else if (event & IN_OPEN         ) eventType = "IN_OPEN         ";
    else eventType = "UNKNOWN";
    fprintf(stdout, "%s %s %s\n", outstr, eventType, filename);
    if (exitOnDelete && (event & IN_DELETE)) exit(0);
}

int main(int argc, char **argv)
{
    int events = IN_ALL_EVENTS; /* Could be |'d of any of the above mentioned events */
    exitOnDelete = getenv("EXIT_ON_DELETE");
    if (argc == 1)
    {
        char *pwd = get_current_dir_name();
        char *dirs[] = { pwd, NULL };
        int r;
        fprintf(stderr, "No arguments given.  Watching %s.\n", pwd);
        r = fileWatch(dirs, events, fileWatchCallback);
        free(pwd);
        return r;
    }
    else
    {
        return fileWatch(&argv[1], events, fileWatchCallback);
    }
}
