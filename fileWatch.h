#ifndef FILEWATCH_H
#define FILEWATCH_H

/*
 * The purpose of this library is to use Linux's inotify API to watch for file
 * events under a directory.
 *
 * Author : Nathan Wharton 10/24/2016
 */

/* The prototype of the callback function that is to be called when any of the specified
 * events happens to any file under the specified directory.
 *
 * The function is passed the absolute filename and the event that occured.
 */

typedef void (*FileWatchCallback)(char *filename, int event);

/*
 * This function loops forever watching for the specified events happening to files under
 * the specified directories (which must be NULL terminated).
 *
 * Possible Events Are:
 * IN_ACCESS         File was accessed (read)
 * IN_ATTRIB         Metadata changed, e.g., permissions, timestamps, extended attributes, link count (since Linux 2.6.25), UID, GID, etc.
 * IN_CLOSE_WRITE    File opened for writing was closed
 * IN_CLOSE_NOWRITE  File not opened for writing was closed
 * IN_CREATE         File/directory created in watched directory
 * IN_DELETE         File/directory deleted from watched directory
 * IN_MODIFY         File was modified
 * IN_MOVED_FROM     Generated for the directory containing the old filename when a file is renamed
 * IN_MOVED_TO       Generated for the directory containing the new filename when a file is renamed
 * IN_OPEN           File was opened
 *
 * Multiple events can be specified by using bit-wise 'or' (the '|' operator)
 *
 */

int fileWatch(char **directories, int events, FileWatchCallback fileWatchCallback);

#endif
