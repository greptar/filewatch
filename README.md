This library provides a function to watch for file system events on files under a given directory.  The events it will report are:
```
IN_ACCESS         File was accessed (read)
IN_ATTRIB         Metadata changed, e.g., permissions, timestamps, extended attributes, link count (since Linux 2.6.25), UID, GID, etc.
IN_CLOSE_WRITE    File opened for writing was closed
IN_CLOSE_NOWRITE  File not opened for writing was closed
IN_CREATE         File/directory created in watched directory
IN_DELETE         File/directory deleted from watched directory
IN_MODIFY         File was modified
IN_MOVED_FROM     Generated for the directory containing the old filename when a file is renamed
IN_MOVED_TO       Generated for the directory containing the new filename when a file is renamed
IN_OPEN           File was opened
```
Multiple events can be specified by using bit-wise 'or' (the '|' operator).

If you use this to watch for a IN_CLOSE_WRITE, you will get the notification after the process writing the file closes it.

A simple example of how to use this library:
```c
static void fileWatchCallback(char *filename, int event)
{
    char outstr[64], *eventType;
    time_t t;
    struct tm *tmp;
    t = time(NULL);
    tmp = localtime(&t);
    strftime(outstr, sizeof(outstr), "%Y/%m/%d %H:%M:%S", tmp);
         if (event & IN_ACCESS       ) eventType = "IN_ACCESS       ";
    else if (event & IN_ATTRIB       ) eventType = "IN_ATTRIB       ";
    else if (event & IN_CLOSE_WRITE  ) eventType = "IN_CLOSE_WRITE  ";
    else if (event & IN_CLOSE_NOWRITE) eventType = "IN_CLOSE_NOWRITE";
    else if (event & IN_CREATE       ) eventType = "IN_CREATE       ";
    else if (event & IN_DELETE       ) eventType = "IN_DELETE       ";
    else if (event & IN_MODIFY       ) eventType = "IN_MODIFY       ";
    else if (event & IN_MOVED_FROM   ) eventType = "IN_MOVED_FROM   ";
    else if (event & IN_MOVED_TO     ) eventType = "IN_MOVED_TO     ";
    else if (event & IN_OPEN         ) eventType = "IN_OPEN         ";
    else eventType = "UNKNOWN";
    fprintf(stdout, "%s %s %s\n", outstr, eventType, filename);
}

int main(int argc, char **argv)
{
    int events = IN_ALL_EVENTS; /* Could be |'d of any of the above mentioned events */
    if (argc == 1)
    {
        char *pwd = get_current_dir_name();
        int r;
        fprintf(stderr, "No arguments given.  Watching %s.\n", pwd);
        r = fileWatch(pwd, events, fileWatchCallback);
        free(pwd);
        return r;
    }
    else
    {
        return fileWatch(argv[1], events, fileWatchCallback);
    }
}
```
