#ifndef INT_TO_PTR_TREE_H
#define INT_TO_PTR_TREE_H

#include "tree.h"

Tree_Type intToPtrTreeAllocate(void);
          /* Creates a new tree */

char *    intToPtrTreeInsert(Tree_Type intToPtrTree, int number, char *pointer);
          /* Returns pointer that was already at number, NULL if this is a new entry */

char *    intToPtrTreeLookup(Tree_Type intToPtrTree, int number);
          /* Returns pointer at number, NULL if missing */

char *    intToPtrTreeRemove(Tree_Type intToPtrTree, int number);
          /* Returns pointer that was at number, NULL if missing */

#endif
