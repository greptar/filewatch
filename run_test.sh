#!/bin/bash
set -e
mkdir d
EXIT_ON_DELETE=true ./sampleWatcher d >& out &
cd d
sleep 1
echo hello > t
sleep 1
mv t ..
sleep 1
mv ../t .
sleep 1
rm t
cd ..
wait
if cut -d ' ' -f 3 out | cmp /dev/stdin test_expected.txt ; then
    echo Pass
else
    echo Output:
    cat out
    echo Expected:
    cat test_expected.txt
    exit 1
fi
